import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/main.dart';


class Sqlite_List extends StatefulWidget {
  @override
  _userinfoState createState() => _userinfoState();
}

class _userinfoState extends State<Sqlite_List> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Of Employees'),
      ),
      body: FutureBuilder<List>(
        future: DatabaseHelper.instance.getAllData(),
        builder: (context, snapshot) {
          if(snapshot.hasData)
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (_, position) {
                return Container(
                  height: 80,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                            "Employee Name:- " + snapshot.data[position].row[1]
                        ),
                        Text(
                            "Employee Rollno:- " + snapshot.data[position].row[2]
                        ),
                        Text(
                            "Employee Phone no:- " + snapshot.data[position].row[3]
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          else
            return Center(
              child: CircularProgressIndicator(),
            );
        },
      ),
    );
  }
}