
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/main.dart';

import 'Sqlite_List.dart';

 void main(){
   runApp(MaterialApp(
       home: form(),
     debugShowCheckedModeBanner: false,
   ));
 }

class form extends StatefulWidget {

  @override
  _Txt_fieldState createState() => _Txt_fieldState();
}

class _Txt_fieldState extends State<form> {

  TextEditingController _name = TextEditingController();
  TextEditingController _roll = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _id = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text('Text field'),
      ),
      body:ListView(
          children:[ Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: _name,
                  decoration: InputDecoration(
                    labelText: 'Enter your name',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: _roll,
                  decoration: InputDecoration(
                    labelText: 'Enter your rollno.',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: _phone,
                  decoration: InputDecoration(
                    labelText: 'Enter your phoneno.',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: _id,
                  decoration: InputDecoration(
                    labelText: 'Enter your id.',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              ButtonBar(
                alignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    child: Text('insert'),
                    onPressed: ()async{
                        int i = await DatabaseHelper.instance.insert({
                           DatabaseHelper.columnName : _name.text,
                          DatabaseHelper.columnroll: _roll.text,
                          DatabaseHelper.columnphone: _phone.text,
                        });
                        print('The inserted $i');
                    },
                  ),
                  RaisedButton(
                    child: Text('update'),
                    onPressed: () async{
                       int updateid = await DatabaseHelper.instance.update({
                          DatabaseHelper.columnId:int.parse(_id.text),
                         DatabaseHelper.columnName:_name.text,
                         DatabaseHelper.columnroll: _roll.text,
                         DatabaseHelper.columnphone: _phone.text,
                       });
                       print(updateid);
                    }
                  ),
                  RaisedButton(
                    child: Text('delete'),
                    onPressed: ()async{
                         int deleteId = await DatabaseHelper.instance.delete(int.parse(_id.text));
                         print(deleteId);
                    },
                  ),
                  RaisedButton(
                    child: Text('get all data'),
                    onPressed: ()async{
                      List<Map<String, dynamic>> data = await DatabaseHelper.instance.getAllData();
                      print(data);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Sqlite_List(),));
                    },
                  )
                ],
              ),
            ],
          ),
          ]
      ),
    );
  }
}
