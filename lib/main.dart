import 'dart:io';
import 'package:flutter_sqlite/form.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{

  DatabaseHelper._();
  static final DatabaseHelper instance = DatabaseHelper._();

  static final _dbName = 'PersonInfo.db';
  static final _tableName = 'Info';
  static final columnId = '_id';
  static final columnName = 'name';
  static final columnroll = 'roll';
  static final columnphone = 'phone';

  static Database _database;

  Future<Database> get database async{
    if(_database != null) return _database;

    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async{
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path,_dbName);
    return  openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async{
    await db.execute('''
    CREATE TABLE $_tableName(
    $columnId INTEGER PRIMARY KEY,
    $columnName TEXT NOT NULL,
    $columnroll TEXT NOT NULL,
    $columnphone TEXT NOT NULL
     )
    '''
    );
  }

  Future<int> insert(Map<String, dynamic> row) async{
    Database db = await instance.database;
    return await db.insert(_tableName, row);
  }


  Future<List<Map<String, dynamic>>> getAllData() async{
    Database db = await instance.database;
    return await db.query(_tableName);
  }

  Future<int> update(Map<String, dynamic> row) async{
    Database db = await instance.database;
    int id = row[columnId];
    return await db.update(_tableName, row, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> delete(int id) async{
    Database db = await instance.database;
    return await db.delete(_tableName, where: '$columnId = ?', whereArgs: [id]);
  }
}